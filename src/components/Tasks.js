import Task from './Task'

const Tasks = ({color, tasks, onDelete, onToggle}) => {

    return (
        <>
        { tasks.map((task) => (
            <Task
                key={task.id}
                style={{ color: color }}
                task={task}
                onDelete={onDelete}
                onToggle={onToggle} />
        )) }

        </>
    )
}

Tasks.defaultProps = {
    color: 'black'
}

export default Tasks
