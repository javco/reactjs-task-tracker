

import PropTypes from 'prop-types'
import Button from './Button'

const Header = ({title, onAdd, showAdd}) => {

    return (
        <header style={headingStyle} className='header'>
            <h1>TaskTracker, {title}</h1>

            <Button
                color={showAdd ? 'red':'green' }
                text={showAdd ? 'Close':'Add' }
                onClick={onAdd}
                />
        </header>
    )
}


Header.defaultProps = {
    title: 'you!'
}

Header.propTypes = {
    title: PropTypes.string,
}

const headingStyle = {
    color: 'orangered'
}

export default Header
