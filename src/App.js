import './App.css';

import { useState, useEffect } from 'react'
import Header from './components/Header'
import Tasks from './components/Tasks'
import AddTask from './components/AddTask'

function App() {

  const name = 'man!'

  const [showAddTask, setShowAddTask] = useState(false)
  const [tasks, setTasks] = useState([])

  useEffect(() => {
    const getTasks = async () => {
      const tasks = await fetchTasks()
      setTasks(tasks)
    }

    getTasks()
  }, [])

  // fetch Tasks
  const fetchTasks = async () => {
    const res = await fetch('http://localhost:5000/tasks')
    const data = await res.json()

    return data
  }

  // add
  const addTask = async (task) => {
    const id = Math.floor(Math.random() * 10000) + 1
    const newTask = {id, ...task}
    setTasks([...tasks, newTask])
  }

  // delete
  const deleteTask = async (id) => {
    console.log('delete', id)
    setTasks(
      tasks.filter((task) => task.id !== id)
      )
  }

  // toggle reminder
  const toggleReminder = (id) => {
    console.log('toggle', id)
    setTasks(
      tasks.map((task) =>
        task.id === id ? {
          ...task, reminder: !task.reminder
        } : task
      )
    )
  }

  return (
    <div className="container">
      <Header title={name}
        onAdd={() => setShowAddTask(!showAddTask)}
        showAdd={showAddTask}
        />
      {showAddTask && <AddTask
        onAdd={addTask}
        />}
      {tasks.length > 0 ? (
        <Tasks tasks={tasks}
          onDelete={deleteTask}
          onToggle={toggleReminder}
          />
      ) : (
        'No results!'
      )}
    </div>
  );
}

export default App;
